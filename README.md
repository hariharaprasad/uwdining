# README #

This README is all about the UWDining side project. The aim of this project is to convert the UWdining Map which is in the form of a PDF file, in the UW HFS website, to a web page. The web page has a Google map displayed along with the type of dining on a side bar. Clicking on a choice in side bar displays the list of dining spots in the campus on the map and on the side bar. Clicking on each spots will animate the marker on the map and displays the details about the selected spot. I keep this page constantly updating by adding new features or styles. I am also planning to make this webpage compatible across browsers and also suitable for mobile devices.

# CREDITS #

* Aptana for Studio editor
* Adobe for Brackets editor
* Google for Google Maps API
* Contributing members of the GeoXML3 parser project
* UW HFS for the Dining Map and the idea for this project
* W3Schools for references to HTML, CSS and JS