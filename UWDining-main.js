function initialize() {
    var uwredsquare = new google.maps.LatLng(47.655878,-122.309507);
    var mapOptions = {
        zoom: 12,
        center: uwredsquare
    }
    map = new google.maps.Map(document.getElementById('uwDiningMap'), mapOptions);
    myParser = new geoXML3.parser({map: map, afterParse: useData, processStyles: true, suppressInfoWindows: true});
  	myParser.parse('UWDiningMapAutumn2014.kml');
};

google.maps.event.addDomListener(window, 'load', initialize);

function changeMarkerMouseOver(place, placeType) {
    var icon = "map_icons/" + placeType + "_selected.png";
    var i;
    for (i = 0; i < myParser.docs[0].placemarks.length; i++) {
        var allPlacesSplit = myParser.docs[0].placemarks[i].name.split("_");
	    var allPlacesCategory = allPlacesSplit[0];
	    var allPlacesName = allPlacesSplit[1];
    	if(allPlacesName == place) {
			myParser.docs[0].placemarks[i].marker.setIcon(icon);
		}
    };
};

function changeMarkerMouseOut(place, placeType) {
    		
    		var icon = "map_icons/" + placeType + ".png";
    		for(var i=0; i<myParser.docs[0].placemarks.length; i++) {
    			
    			var allPlacesSplit = myParser.docs[0].placemarks[i].name.split("_");
	    		var allPlacesCategory = allPlacesSplit[0];
	    		var allPlacesName = allPlacesSplit[1];
    			
				if(allPlacesName == place) {
					myParser.docs[0].placemarks[i].marker.setIcon(icon);
				};
			};
    	};
    
function showMarkerInfo(place, placeType) {
    		
    		var animation = google.maps.Animation.BOUNCE;
    		for(var i=0; i<myParser.docs[0].placemarks.length; i++) {
    			
    			var allPlacesSplit = myParser.docs[0].placemarks[i].name.split("_");
	    		var allPlacesCategory = allPlacesSplit[0];
	    		var allPlacesName = allPlacesSplit[1];
    			
				if(allPlacesName == place) {
					myParser.docs[0].placemarks[i].marker.setAnimation(animation);
					console.log(myParser.docs[0].placemarks[i]);
					//var mapElement = document.getElementById("map-canvas");
					document.getElementById("uwDiningMap").style.width = '55%';
					document.getElementById("info").innerHTML = myParser.docs[0].placemarks[i].description;
					document.getElementById("info").style.display = '';
					document.getElementById("infoWindow").style.display = '';
				}
				else {
					myParser.docs[0].placemarks[i].marker.setAnimation(null);
				}
			};
    	};
    	
function displayMarkers(places,setVisibility){
    		for(var i=0; i<myParser.docs[0].placemarks.length; i++) {
    			
    			var allPlacesSplit = myParser.docs[0].placemarks[i].name.split("_");
	    		var allPlacesCategory = allPlacesSplit[0];
	    		var allPlacesName = allPlacesSplit[1];
	    		//console.log(places,allPlacesCategory)
    			
				if(allPlacesCategory == places && setVisibility == true) {
					myParser.docs[0].placemarks[i].marker.setVisible(true);
					//console.log(myParser.docs[0].placemarks[i].marker);
				}
				else {
					if(allPlacesCategory == places && setVisibility == false) {
						myParser.docs[0].placemarks[i].marker.setVisible(false);
					}
					else {
						myParser.docs[0].placemarks[i].marker.setVisible(false);
					}
				};
			};
    	};
    	
function showMap(places){
			
			var listElement = document.getElementById('UWDiningList').childNodes;
			
			for (var i=0;i<listElement.length;i++) {
				if(listElement[i].nodeName == 'LI') {
					if(listElement[i].id == places && document.getElementById(listElement[i].id).childNodes[3].style.display == 'none') {
						document.getElementById(listElement[i].id).childNodes[3].style.display = "";
						displayMarkers(places,true);
						document.getElementById("uwDiningMap").style.width = '75%';
						document.getElementById("info").style.display = 'none';
						document.getElementById("infoWindow").style.display = 'none';
					}
					else {
						if(listElement[i].id == places && document.getElementById(listElement[i].id).childNodes[3].style.display == '') {
							document.getElementById(listElement[i].id).childNodes[3].style.display ="none";
							displayMarkers(places,false);
							document.getElementById("uwDiningMap").style.width = '75%';
							document.getElementById("info").style.display = 'none';
							document.getElementById("infoWindow").style.display = 'none';
						}
						else {
							document.getElementById(listElement[i].id).childNodes[3].style.display = "none";
						}
					};
				}
			}
    		return false;
    	};
    	
function useData(docs){
    		myParser.hideDocument(docs[0]);
    		document.getElementById("uwDiningMap").style.width = '75%';
			document.getElementById("info").style.display = 'none';
			document.getElementById("infoWindow").style.display = 'none';
			
    		var allPlaces = docs[0].placemarks;
    		var listElement = document.getElementById('UWDiningList').childNodes;
    		
    		for (i=0;i<listElement.length;i++) {
    			tempPlace = listElement[i].id;
    			
    			if(listElement[i].nodeName == 'LI'){
	    			var placeListUl = document.createElement('ul');
	    			var placeListId = tempPlace + " list"; 
	    			placeListUl.setAttribute("id",placeListId);
	    			var placeList = "";
	    		
	    			for(var j=0;j<allPlaces.length;j++){
	    				var allPlacesSplit = allPlaces[j].name.split("_");
	    				var allPlacesCategory = allPlacesSplit[0];
	    				var allPlacesName = allPlacesSplit[1];
	    				if(allPlacesCategory == tempPlace){
	    					placeList += "<li id=\"" + allPlacesName + "\">";
	    					placeList += "<a href=\"#\" onclick=\"showMarkerInfo(this.parentNode.id,this.parentNode.parentNode.parentNode.id)\" onmouseover=\"changeMarkerMouseOver(this.parentNode.id,this.parentNode.parentNode.parentNode.id)\" onmouseout=\"changeMarkerMouseOut(this.parentNode.id,this.parentNode.parentNode.parentNode.id)\">";
	    					placeList += allPlacesName;
	    					placeList += "</a></li>";
	    				}
	    			};
	    			
	    			placeListUl.innerHTML = placeList;
	    			document.getElementById(tempPlace).appendChild(placeListUl).style.display = 'none';
    			};
    		};
		};
    	
        
